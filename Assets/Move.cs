﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
  private int speed = 5;
  private int leftLimit = -10;
  private int rightLimit = 10;

  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    if (transform.position.x <= leftLimit || transform.position.x >= rightLimit)
    {
      speed = -1 * speed;
    }

    transform.position = transform.position + new Vector3(speed * Time.deltaTime, 0, 0);
  }
}
