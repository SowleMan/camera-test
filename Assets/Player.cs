﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
  private CharacterController characterController;

  [SerializeField]
  private float forwaredMoveSpeed = 7.5f;
  [SerializeField]
  private float backwardMoveSpeed = 3;
  [SerializeField]
  private float turnSpeed = 150f;
  // Start is called before the first frame update
  void Start()
  {
    characterController = GetComponent<CharacterController>();
  }

  // Update is called once per frame
  void Update()
  {
    var horizontal = Input.GetAxis("Horizontal");
    var vertical = Input.GetAxis("Vertical");

    var movement = new Vector3(horizontal, 0, vertical);

    transform.Rotate(Vector3.up, horizontal * turnSpeed * Time.deltaTime);

    if (vertical != 0)
    {
      float moveSpeedToUse = vertical > 0 ? forwaredMoveSpeed : backwardMoveSpeed;
      characterController.SimpleMove(transform.forward * moveSpeedToUse * vertical);
    }

    // characterController.SimpleMove(movement * Time.deltaTime * moveSpeed);

    // animator.SetFloat("Speed", movement.magnitude);

    // if (movement.magnitude > 0)
    // {
    //   Quaternion newDirection = Quaternion.LookRotation(movement);
    //   transform.rotation = Quaternion.Slerp(transform.rotation, newDirection, Time.deltaTime * turnSpeed);
  }
}
